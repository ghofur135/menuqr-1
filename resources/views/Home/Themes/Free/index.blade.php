<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets1/css/bootstrap.min.css">
        <!-- Animate CSS -->
        <link rel="stylesheet" href="assets1/css/animate.min.css">
        <!-- Magnific CSS -->
        <link rel="stylesheet" href="assets1/css/magnific-popup.css">
        <!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="assets1/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets1/css/owl.theme.default.min.css">
        <!-- Line Awesome CSS -->
        <link rel="stylesheet" href="assets1/css/line-awesome.min.css">
        <!-- Odometer CSS -->
        <link rel="stylesheet" href="assets1/css/odometer.css">
        <!-- Stylesheet CSS -->
        <link rel="stylesheet" href="assets1/css/style.css">
        <!-- Stylesheet Responsive CSS -->
        <link rel="stylesheet" href="assets1/css/responsive.css">
        <!-- Favicon -->
        <link rel="icon" type="images/png" href="assets1/images/favicon.png">
        <!-- Title -->
        <title>ScanMenuMu - #ayobikinmudah #scanaja</title>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-4KZ7WM8J2N"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-4KZ7WM8J2N');
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-199991683-1">
        </script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-199991683-1');
        </script>
    </head>

    <body data-spy="scroll" data-offset="70">

        <!-- Preloader -->
        <!--
        <div class="preloader">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="lds-hourglass"></div>
                </div>
            </div>
        </div>
        -->
        <!-- End Preloader -->

        <!-- Nabvar Area -->
        <nav class="navbar fixed-top navbar-expand-lg main-navbar app-nav">
            <div class="container">
                <a class="navbar-brand" href="app-landing.html">
                    <img src="assets1/images/logo.png" alt="logo">
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar top-bar"></span>
                    <span class="icon-bar middle-bar"></span>
                    <span class="icon-bar bottom-bar"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#home">Beranda</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#steps">Coba Yuk</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#features">Fitur</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#screenshots">Mokup</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../pricing.html">Harga</a>
                        </li>
                        <!--
                        <li class="nav-item">
                            <a class="nav-link" href="#testimonials">Testimonials</a>
                        </li>
                        -->
                    </ul>
                    <div class="nav-btn">
                        <a href="https://app.scanmenumu.com/store/auth/login" class="default-btn bg-main">Login</a>
                    </div>
                </div>
            </div>
        </nav>
        <!-- End Nabvar Area -->

        <!-- App Landing Banner Area -->
        <div id="home" class="app-banner-area pt-100">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="app-bennr-text">
                            <span>Brand New App 2021</span>
                            <h1>Manajemen Resto System Integrasi</h1>
                            <p>Mudahkan kebutuhan pelanggan ketika datang ke Resto. Cukup duduk di meja, scan barcode, pilih menu dan siap makan.</p>
                            <div class="app-btn">
                                <a href="https://app.scanmenumu.com/store/register" class="default-btn app-btn-1 mr-3">Daftar</a>
                                <a href="https://app.scanmenumu.com/store/demo1" class="default-btn app-btn-2">Demo</a>
                            </div>
                            <br>
<!--
                            <div class="row mt-30">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="app-text">
                                        <h3>50.6k</h3>
                                        <p>App <br> Download</p>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-sm-6">
                                    <div class="app-text">
                                        <h3>16.2k</h3>
                                        <p>Positive <br> Reviews</p>
                                    </div>
                                </div>
                            </div> <--
-->
                            <div class="app-shapes">
                                <img src="assets1/images/shape/shape1.png" class="shape-1" alt="Image">
                                <img src="assets1/images/shape/shape30.png" class="shape-30"  alt="Image">
                                <img src="assets1/images/shape/shape28.png" class="shape-28"  alt="Image">
                                <img src="assets1/images/shape/shape16.png" class="shape-16"  alt="Image">
                                <img src="assets1/images/shape/shape16.png" class="shape-11"  alt="Image">
                                <img src="assets1/images/shape/shape14.png" class="shape-14"  alt="Image">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="app-banner-img">
                            <img src="assets1/images/app-img/app-banner-img.png" class="app-img" alt="Image">
                            <div class="app-shapes">
                                <img src="assets1/images/shape/shape31.png" class="shape-31"  alt="Image">
                                <img src="assets1/images/shape/shape4.png" class="shape-4"  alt="Image">
                                <img src="assets1/images/shape/shape2.png" class="shape-2"  alt="Image">
                                <img src="assets1/images/shape/shape22.png" class="shape-22"  alt="Image">
                                <img src="assets1/images/shape/shape5.png" class="shape-5"  alt="Image">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-shape">
                    <img src="assets1/images/shape/shape29.png" alt="Image">
                </div>
            </div>
        </div>
        <!-- End App Landing Banner Area -->

        <!-- Easy Steps Area -->
        <div id="steps" class="easy-step-area ptb-100">
            <div class="container">
                <div class="section-title">
                    <span>Langkah Mudah</span>
                    <h2>ScanMenuMu Gratis 90 hari</h2>
                    <p>ScanMenuMu memberikan free trial 90 hari agar kamu bisa memaksimalkan digitalisasi Restomu.</p>
                </div>

                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="easy-step-img">
                            <img src="assets1/images/app-img/easy-step-1.png" class="step-1" alt="Image">
                            <div class="easy-step-shape">
                                <img src="assets1/images/shape/shape1.png" class="shape-1" alt="Shape">
                                <img src="assets1/images/shape/shape21.png" class="shape-21" alt="Shape">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="easy-step-card left-text">
                            <div class="row align-items-right">
                                <div class="col-lg-4 col-sm-6">
                                    <div class="step-signle-card">
                                        <a class="bg-1" href="https://app.scanmenumu.com/store/register" target="_blank">
                                        <i class="las la-rocket bg-1"></i>
                                        </a>
                                        <span>1</span>
                                        <h3>Klik Daftar</h3>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-sm-6">
                                    <p>Kamu cukup klik tombol daftar, untuk coba gratis ScanMenuMu hingga 90 hari.</p>
                                </div>
                            </div>
                        </div>

                        <div class="easy-step-card left-text">
                            <div class="row align-items-right">
                                <div class="col-lg-4 col-sm-6">
                                    <div class="step-signle-card">
                                        <i class="las la-hotel bg-2"></i>
                                        <span>2</span>
                                        <h3>Atur Resto</h3>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-sm-6">
                                    <p>Setelah proses pendaftaran selesai, langsung atur ScanMenuMu sesuai kebutuhan Restomu.</p>

                                </div>
                            </div>
                        </div>

                        <div class="easy-step-card left-text">
                            <div class="row align-items-right">
                                <div class="col-lg-4 col-sm-6">
                                    <div class="step-signle-card step-p">
                                        <i class="las la-globe bg-3"></i>
                                        <span>3</span>
                                        <h3>Publis!</h3>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-sm-6">
                                    <p>Dengan ScanMenuMu Restomu sekarang menjadi Digital dan tentu saja Online untuk siapapun.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Easy Steps Area -->

        <!-- Our Services Area -->
        <div id="features" class="our-services-area service-shape pt-100 pb-70">
            <div class="container">
                <div class="section-title">
                    <span>Fitur Unggulan</span>
                    <h2>Cuma ada di ScanMenuMu</h2>
                    <p>Berikut adalah fitur unggulan ScanMenuMu, yang memudahkan pengelolaan Restomu</p>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        <div class="our-service-card">
                            <i class="las la-medkit bg-1"></i>
                            <h3>Aman & Nyaman</h3>
                            <p>Saat Pandemi seperti ini, ScanMenuMu merupakan pilihan yang tepat, Mendukung Protokol kesehatan dengan Meminimalisir kontak fisik dan tetap Social Distancing Menggunakan teknologi terkini tinggal Scan QR Code lalu menu Resto akan tersedia.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-6">
                        <div class="our-service-card">
                            <i class="las la-mobile-alt bg-2"></i>
                            <h3>Tanpa Download Aplikasi</h3>
                            <p>Arahkan Ponselmu ke QR Code yang tersedia, pelanggan bisa langsung memesan menu yang diinginkan!.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-6">
                        <div class="our-service-card">
                            <i class="las la-bolt bg-3"></i>
                            <h3>Mudah, Cepat, dan Fleksibel</h3>
                            <p>Tidak memerlukan keahlian khusus, semua orang bisa cepat memahami dan menggunakan Bisa langsung integrasikan Menu Restoranmu ke ScanMenumu.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-6">
                        <div class="our-service-card">
                            <i class="las la-certificate bg-4"></i>
                            <h3>Modern & Teknologi terkini</h3>
                            <p>Tidak perlu membuang banyak waktu untuk mengantarkan buku menu ke masing-masing meja Cukup Scan QR Code lalu bisa langsung memesan menu yang diinginkan. Tanpa Ribet, Tanpa Bingung. Customer bisa mengetahui proses orderan yang dipesan melalui ScanMenuMu.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-6">
                        <div class="our-service-card">
                            <i class="las la-headset bg-5"></i>
                            <h3>Dukungan Layanan</h3>
                            <p>Tidak perlu khawatir, tim ScanMenuMu akan membuatmu lebih mudah dalam mengelola Restomu.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-6">
                        <div class="our-service-card">
                            <i class="las la-hand-holding-usd bg-6"></i>
                            <h3>NO Fee</h3>
                            <p>Dengan ScanMenuMu kamu tidak perlu memberikan fee dari hasil penjualan.</p>
                        </div>
                    </div>
                </div>
                <div class="our-service-shape">
                    <img src="assets1/images/shape/shape26.png" alt="Shape">
                </div>
            </div>
        </div>
        <!-- End Our Services Area -->

        <!-- Popup Video Area -->

        <div class="popup-video-area bg-2">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="popup-video">
                            <div class="video-btn">
                                <a href="https://www.youtube.com/watch?v=CpLWruHKSlk" class="popup-youtube">
                                    <i class="las la-play"></i>
                                    <span class="ripple pinkBg"></span>
                                    <span class="ripple pinkBg"></span>
                                    <span class="ripple pinkBg"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- End Popup Video Area -->

        <!-- Powerful Area -->
        <div class="powerful-area pt-100">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="section-title">
                            <h2>Fitur ScanMenuMu di Resto</h2>
                        </div>
                        <div class="powerful-card">
                            <i class="las la-layer-group bg-1"></i>
                            <h3>Dasbor Resto</h3>
                            <p>Disini informasi pesanan baru untuk Restomu akan tampil, segera proses pesanananya ya</p>
                        </div>

                        <div class="powerful-card">
                            <i class="las la-tv bg-2"></i>
                            <h3>Layar Status Pesanan</h3>
                            <p>Informasi ini sangat berguna bagi pelanggan untuk mengetahui status pesanan (Diterima, Proses, Siap Hidangkan).</p>
                        </div>

                        <div class="powerful-card">
                            <i class="las la-bell bg-3"></i>
                            <h3>Panggil Petugas</h3>
                            <p>Fitur ini memudahkan pelangganmu untuk menghubungi petugas, tanpa harus memanggil.</p>
                        </div>

                        <div class="powerful-card">
                            <i class="las la-heartbeat bg-4"></i>
                            <h3>Mendukung Prokes</h3>
                            <p>ScanMenuMu berkomitmen untuk selalu mendukung aturan prokes, mengurangi penyebaran covid19.</p>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="powerful-img">
                            <img src="assets1/images/app-img/powerful.png" alt="Image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Powerful Area -->

        <!-- Perfect Area -->
        <div class="perfect-area ptb-100">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="perfect-img">
                            <img src="assets1/images/app-img/powerfull.png" class="perfect-1" alt="Image">
                            <div class="perfect-shape">
                                <img src="assets1/images/shape/shape25.png" alt="Image">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-7">
                        <div class="section-title">
                            <h2>Yuk Cobain gimana ScanMenuMu</h2>
                            <p>ScanMenuMu membantu pelanggan memilih menu cukup dengan scan barcode saja.</p>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-sm-6">
                                <div class="perfect-card">
                                    <i class="las la-camera"></i>
                                    <h3>Buka Kamera HPmu</h3>
                                    <p>Ya, betul cukup menggunakan kamera hpmu arahkan ke barcode.</p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-sm-6">
                                <div class="perfect-card">
                                    <i class="las la-file-alt"></i>
                                    <h3>Pilih Menu</h3>
                                    <p>Kamu bisa langsung pilih menu yang disediakan Resto.</p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-sm-6">
                                <div class="perfect-card">
                                    <i class="las la-bolt"></i>
                                    <h3>Cepat dan Mudah</h3>
                                    <p>Pesananmu langsung masuk di Resto, dan kamu bisa cek status pesanan.</p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-sm-6">
                                <div class="perfect-card">
                                    <i class="las la-hamburger"></i>
                                    <h3>Silakan Dinikmati</h3>
                                    <p>Yeay..kamu sudah terima makanan pesananmu, silakan menikmati.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Perfect Area -->

        <!-- Screens Slider Area -->
        <div id="screenshots" class="screens-slider-area ptb-100">
            <div class="container">
                <div class="section-title">
                    <span>ScanMenuMu Mokup</span>
                    <h2>Modern, Simple, Mudah</h2>
                    <p>Yuk intip seperti apa sih Menu Digital dari ScanMenuMu.</p>
                </div>

                <div class="screens-slider owl-carousel owl-theme">
                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot1.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot2.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot3.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot4.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot5.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot2.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot4.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot3.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot5.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot1.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot4.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot5.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot2.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot4.png" alt="Image">
                    </div>

                    <div class="screen-slider-item">
                        <img src="assets1/images/app-img/screenshot3.png" alt="Image">
                    </div>
                </div>
            </div>
        </div>
        <!-- End Screens Slider Area -->

        <!-- App Features Area -->
<!--        <div class="app-features-area">
            <div class="container">
                <div class="section-title">
                    <span>App Download</span>
                    <h2>Download The App With Top Features</h2>
                    <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account.</p>
                </div>
                <div class="app-features-img">
                    <a href="#">
                        <img src="assets1/images/app-img/google-play.png" class="mr-3" alt="Image">
                    </a>
                    <a href="#">
                        <img src="assets1/images/app-img/app-store.png" alt="Image">
                    </a>
                </div>
                <div class="app-features">
                    <img src="assets1/images/app-img/app-features.png" alt="Image">
                </div>
            </div>
        </div>
-->
        <!-- End App Features Area -->

        <!-- Testimonials Silder Two Area -->
<!--        <div id="testimonials" class="clients-slider-two-area ptb-100">
            <div class="container">
                <div class="section-title">
                    <span>Clients Review</span>
                    <h2>Our App Users And Their Experience</h2>
                    <p>"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores.</p>
                </div>
                <div class="clients-slider-two owl-carousel owl-theme">
                    <div class="slider-two-item">
                        <img src="assets1/images/portfolio/client1.jpg" alt="Image">
                        <h3>Thomas Smith</h3>
                        <span>CEO Of LTD Company</span>
                        <div class="rating">
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                        </div>
                        <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire.</p>
                        <i class="las la-quote-left clients-icon"></i>
                    </div>

                    <div class="slider-two-item">
                        <img src="assets1/images/portfolio/client2.jpg" alt="Image">
                        <h3>Sinthy Alina</h3>
                        <span>Marking Assistant</span>
                        <div class="rating">
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                        </div>
                        <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire.</p>
                        <i class="las la-quote-left clients-icon"></i>
                    </div>

                    <div class="slider-two-item">
                        <img src="assets1/images/portfolio/client3.jpg" alt="Image">
                        <h3>Jehson Benther</h3>
                        <span>Shipping Manager</span>
                        <div class="rating">
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                        </div>
                        <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire.</p>
                        <i class="las la-quote-left clients-icon"></i>
                    </div>

                    <div class="slider-two-item">
                        <img src="assets1/images/portfolio/client4.jpg" alt="Image">
                        <h3>Niccy Priti</h3>
                        <span>Founder - CEO</span>
                        <div class="rating">
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                        </div>
                        <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire.</p>
                        <i class="las la-quote-left clients-icon"></i>
                    </div>

                    <div class="slider-two-item">
                        <img src="assets1/images/portfolio/client2.jpg" alt="Image">
                        <h3>Niccy Priti</h3>
                        <span>Founder - CEO</span>
                        <div class="rating">
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                            <i class="las la-star"></i>
                        </div>
                        <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire.</p>
                        <i class="las la-quote-left clients-icon"></i>
                    </div>
                </div>
                <div class="client-shape">
                    <img src="assets1/images/shape/shape27.png" alt="Image">
                </div>
            </div>
        </div>
    -->
        <!-- End Testimonials Silder Two Area -->

        <!-- Subscribe Area -->
        <!--
        <div class="subscribe-area mt-minus-100">
            <div class="container">
                <div class="subscribe-contant">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <div class="subscrive-text">
                                <h3>Subscribe Our Newsletter</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="subscribe-form">
                                <form class="newsletter-form" data-toggle="validator">
                                    <input type="email" class="form-control" placeholder="Enter your email" name="EMAIL" required="" autocomplete="off">
                                    <button type="submit" class="btn btn-primary">Subscribe</button>

                                    <div id="validator-newsletter" class="form-result"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        -->
        <!-- End Subscribe Area -->

        <!-- Footer Area -->
        <footer class="footer-area pt-200">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        <div class="footer-widget">
                            <div class="logo">
                                <img src="assets1/images/logo-2.png" alt="ScanMenuMu">
                            </div>
                            <p>ScanMenuMu - Digital Resto #ayobikinmudah #scanaja.</p>

                            <ul class="footer-social">
                                <li>
                                    <a class="bg-1" href="https://linktr.ee/scanmenumu" target="_blank">
                                        <i class="lab la-globe-asia bg-1"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="bg-2" href="https://www.youtube.com/channel/UC1dRsvcriTlEnjN8errtpKQ" target="_blank">
                                        <i class="lab la-youtube bg-2"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="bg-3" href="https://wa.me/6289675062040/?text=Mau%20coba%20ScanMenuMu%20dong." target="_blank">
                                        <i class="lab la-whatsapp bg-3"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="bg-4" href="https://instagram.com/scanmenumu" target="_blank">
                                        <i class="lab la-instagram bg-4"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="bg-4" href="https://facebook.com/Scanmenumu" target="_blank">
                                        <i class="lab la-facebook bg-4"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-5 col-sm-6">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6">
                                <div class="footer-widget">
                                    <h3 class="title">Spotlight:</h3>
                                    <ul class="footer-text">
                                        <li>
                                            <a href="https://app.scanmenumu.com/store/about">
                                                <i class="las la-angle-right"></i>
                                                Tentang Kami
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://app.scanmenumu.com/store/privacy">
                                                <i class="las la-angle-right"></i>
                                                Kebijakan
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://wa.me/6289675062040/?text=Mau%20coba%20ScanMenuMu%20dong.">
                                                <i class="las la-angle-right"></i>
                                                Hubungi Kami
                                            </a>
                                        </li>
                                        </ul>
                                </div>
                            </div>
<!--
                            <div class="col-lg-6 col-sm-6">
                                <div class="footer-widget">
                                    <h3 class="title">Quick Links</h3>
                                    <ul class="footer-text">
                                        <li>
                                            <a href="#">
                                                <i class="las la-angle-right"></i>
                                                Home
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="las la-angle-right"></i>
                                                Features
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="las la-angle-right"></i>
                                                Pricing
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="las la-angle-right"></i>
                                                Screenshot
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="las la-angle-right"></i>
                                                Testimonial
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> -->
<!--
                    <div class="col-lg-3 col-sm-6 offset-sm-4 offset-lg-0">
                        <div class="footer-widget">
                            <h3 class="title">Download Link</h3>
                            <div class="footer-image">
                                <a href="#">
                                    <img src="assets1/images/app-img/google-play.png" alt="Image">
                                </a>
                                <a href="#">
                                    <img src="assets1/images/app-img/app-store.png" alt="Image">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            -->
                <div class="copyright-text">
                    <p>Copyright @2021 ScanMenuMu All Rights Reserved <a href="https://scanmenumu.com/" target="_blank">ScanMenuMu</a></p>
                </div>
            </div>
        </footer>
        <!-- End Footer Area -->

        <!-- Go Top -->
        <div class="go-top">
            <i class="las la-angle-double-up"></i>
        </div>
        <!-- End Go Top -->

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="assets1/js/jquery.min.js"></script>
        <script src="assets1/js/popper.min.js"></script>
        <script src="assets1/js/bootstrap.min.js"></script>
        <!-- Owl carousel JS -->
        <script src="assets1/js/owl.carousel.min.js"></script>
        <!-- Magnific JS -->
        <script src="assets1/js/jquery.magnific-popup.min.js"></script>
        <!-- Wow JS -->
        <script src="assets1/js/wow.min.js"></script>
        <!-- Odometer JS -->
        <script src="assets1/js/odometer.min.js"></script>
        <!-- Jquery Apper JS -->
        <script src="assets1/js/jquery.appear.js"></script>
        <!-- Form Validator JS -->
		<script src="assets1/js/form-validator.min.js"></script>
		<!-- Contact JS -->
		<script src="assets1/js/contact-form-script.js"></script>
		<!-- Ajaxchimp JS -->
		<script src="assets1/js/jquery.ajaxchimp.min.js"></script>
        <!-- Custom JS -->
        <script src="assets1/js/custom.js"></script>

    <script src="https://apps.elfsight.com/p/platform.js" defer></script>
    <div class="elfsight-app-14908441-a560-4db7-8d11-8c214ab28a9c"></div>

    </body>
</html>
