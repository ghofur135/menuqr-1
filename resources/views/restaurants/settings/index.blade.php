@extends("restaurants.layouts.restaurantslayout")

@section("restaurantcontant")


    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="left-side-tabs">
                    <div class="dashboard-left-links">
                        <a href="#" class="user-item active">Store Settings</a>


                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-6">
                <div class="box_order_view card-static-2 mb-30">

                    <div class="head-settings">
                        <div class="title">
                            <h3>Site Setting</h3>
                        </div>
                    </div>

                    <div class="card-body">
                        @if(session()->has("MSG"))
                            <div class="alert alert-{{session()->get("TYPE")}}">
                                <strong> <a>{{session()->get("MSG")}}</a></strong>
                            </div>
                        @endif
                        @if($errors->any()) @include('admin.admin_layout.form_error') @endif

                        <form class="form-horizontal" method="post" action="{{route('store_admin.update_store_settings')}}" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-username">Store Name</label>
                                            <input type="text"  name="store_name" class="form-control" value="{{$store->store_name}}" placeholder="Store Name" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-email">Email address</label>
                                            <input type="email"  name="email" class="form-control" value="{{$store->email}}" placeholder="Email address" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-username">New Password</label>
                                            <input type="password"  name="password" class="form-control" placeholder="New Password">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-first-name"> Phone Number<small>(with country code:Eg:+91)</small></label>
                                            <input type="text"  class="form-control" value="{{$store->phone}}" placeholder="Phone Number(with country code:Eg:+91)" name="phone" required />
                                        </div>
                                    </div>

                                </div>
                                <div class="row">


                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-first-name">Subscription end date</label>
                                            <input type="text"  class="form-control" value="{{$store->subscription_end_date}}" readonly disabled />
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label text-danger">Store Logo (358px X 358px)</label>
                                            <input type="file" name="logo_url"  class="form-control ui-autocomplete-input" placeholder="Application Logo ()" autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-first-name">Store Currency</label>
                                            <input type="text" name="currency_symbol" class="form-control" value="{{$store->currency_symbol ?? ''}}"  />
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-first-name">Store Service Charge</label>
                                            <input type="text" name="service_charge" class="form-control" value="{{$store->service_charge ?? ''}}"  />
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-first-name">Store Tax(%)</label>
                                            <input type="text"  name="tax" class="form-control" value="{{$store->tax ?? ''}}"  />
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="row">


                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">Address</label>
                                    <textarea rows="4" name="address" class="form-control" >{{$store->address}}</textarea>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">description</label>
                                    <textarea rows="4" name="description"  class="form-control" >{{$store->description}}</textarea>
                                </div>
                            </div>

                            </div>


                            <h6 class="heading-small text-muted mb-4">App Settings <span class="text-danger"> (Enable/Desable)</span></h6>
                            <div class="pl-lg-4">
                                <div class="row">


                                    <div class="col-lg-2">
                                        <div class="form-group">

                                            <label class="form-control-label">Accept order</label><br>
                                            <label class="custom-toggle">
                                                <input type="checkbox" {{$store->is_accept_order? "checked":null}} name="is_accept_order">
                                                <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                                            </label>
                                        </div>
                                    </div>


                                    <div class="col-lg-2">
                                        <div class="form-group">

                                            <label class="form-control-label">Search Section</label><br>
                                            <label class="custom-toggle">
                                                <input type="checkbox" {{$store->search_enable? "checked":null}} name="search_enable">
                                                <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                                            </label>
                                        </div>
                                    </div>


                                    <div class="col-lg-2">
                                        <div class="form-group">

                                            <label class="form-control-label">Language Section</label><br>
                                            <label class="custom-toggle">
                                                <input type="checkbox" {{$store->language_enable? "checked":null}} name="language_enable">
                                                <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">

                                            <label class="form-control-label">Send Order to Whatsapp</label><br>
                                            <label class="custom-toggle">
                                                <input type="checkbox" {{$store->whatsappbutton_enable	? "checked":null}} name="whatsappbutton_enable">
                                                <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                                            </label>
                                        </div>
                                    </div>


                                    <div class="col-lg-2">
                                        <div class="form-group">

                                            <label class="form-control-label">Table</label><br>
                                            <label class="custom-toggle">
                                                <input type="checkbox" {{$store->table_enable	? "checked":null}} name="table_enable">
                                                <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                                            </label>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default btn-flat m-b-30 m-l-5 bg-primary border-none m-r-5 -btn">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection
